# Menú
- [Que es javascript](#Que-es-javascript)
- [Comandos basicos de javascript](#Comandos-basicos-de-javascript)
- [Ejemplos de HELLO WORD](#Ejemplos_de_HELLO_WORD)


# Que es javascript

JavaScript es un lenguaje de programación o de secuencias de comandos que te permite implementar funciones complejas en páginas web, cada vez que una página web hace algo más que sentarse allí y mostrar información estática para que la veas, muestra oportunas actualizaciones de contenido, mapas interactivos, animación de Gráficos 2D/3D, desplazamiento de máquinas reproductoras de vídeo, etc., puedes apostar que probablemente JavaScript está involucrado. Es la tercera capa del pastel de las tecnologías web estándar, dos de las cuales (HTML y CSS) hemos cubierto con mucho más detalle en otras partes del Área de aprendizaje.

## Siginificado de HTML, CSS y JAVASCRIPT

- HTML es el lenguaje de marcado que usamos para estructurar y dar significado a nuestro contenido web, por ejemplo, definiendo párrafos, encabezados y tablas de datos, o insertando imágenes y videos en la página.
- CSS es un lenguaje de reglas de estilo que usamos para aplicar estilo a nuestro contenido HTML, por ejemplo, establecer colores de fondo y tipos de letra, y distribuir nuestro contenido en múltiples columnas.
- JavaScript es un lenguaje de secuencias de comandos que te permite crear contenido de actualización dinámica, controlar multimedia, animar imágenes y prácticamente todo lo demás. (Está bien, no todo, pero es sorprendente lo que puedes lograr con unas pocas líneas de código JavaScript).

# Comandos basicos de javascript

### VARIABLE
Las Variables son contenedores en los que puedes almacenar valores. Primero debes declarar la variable con la palabra clave var (menos recomendado) o let, seguida del nombre que le quieras dar. Se recomienda más el uso de let que de var (más adelante se profundiza un poco sobre esto):

    let nombreDeLaVariable;
----------------
    let nombreDeLaVariable = 'Bob';
    nombreDeLaVariable = 'Steve';

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/javascript/imagenes/VARIABLE.png" width=500>

### COMENTARIOS

Puedes escribir comentarios entre el código JavaScript, igual que puedes en CSS. El navegador ignora el texto marcado como comentario. En JavaScript, los comentarios de una sola línea se escriben así:

    /*
    Esto es un comentario
    de varias líneas.
    */

### OPERACIONES
Un operador es básicamente un símbolo matemático que puede actuar sobre dos valores (o variables) y producir un resultado. En la tabla de abajo aparecen los operadores más simples, con algunos ejemplos para probarlos en la consola del navegador.

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/javascript/imagenes/OPERACIONES.png" width=500>

### CONDICIONES
Las condicionales son estructuras de código que permiten comprobar si una expresión devuelve true o no, y después ejecuta un código diferente dependiendo del resultado. La forma de condicional más común es la llamada if... else. Entonces, por ejemplo:

    let helado = 'chocolate';
    if (helado === 'chocolate') {
    alert('¡Sí, amo el helado de chocolate!');
    } else {
    alert('Awwww, pero mi favorito es el de chocolate...');
    }

### FUNCIONES

Las funciones son una manera de encapsular una funcionalidad que quieres reutilizar, de manera que puedes llamar esa función con un solo nombre, y no tendrás que escribir el código entero cada vez que la utilices. Ya has visto algunas funciones más arriba, por ejemplo:

    let nombreDeLaVariable = document.querySelector('h1');

    alert('¡Hola!');
--------------
    function multiplica(num1,num2) {
      let resultado = num1 * num2;
      return resultado;
    }

### EVENTOS
Para crear una interacción real en tu sitio web, debes usar eventos. Estos son unas estructuras de código que captan lo que sucede en el navegador, y permite que en respuesta a las acciones que suceden se ejecute un código. El ejemplo más obvio es un clic (click event), que se activa al hacer clic sobre algo. Para demostrar esto, prueba ingresando lo siguiente en tu consola, luego da clic sobre la página actual:

    document.querySelector('html').onclick = function() {
        alert('¡Ouch! ¡Deja de pincharme!');
    }

------
    let miHTML = document.querySelector('html');

    miHTML.onclick = function(){};


# Ejemplos de HELLO WORD

La sección de arriba suena realmente emocionante, y debería serlo. JavaScript es una de las tecnologías web más emocionantes, y cuando comiences a ser bueno en su uso, tus sitios web entrarán en una nueva dimensión de energía y creatividad.

Sin embargo, sentirse cómodo con JavaScript es un poco más difícil que sentirse cómodo con HTML y CSS. Deberás comenzar poco a poco y continuar trabajando en pasos pequeños y consistentes. Para comenzar, mostraremos cómo añadir JavaScript básico a tu página, creando un «¡Hola Mundo!» de ejemplo (el estándar en los ejemplos básicos de programación).



1.- Primero, ve a tu sitio de pruebas y crea una carpeta llamada *scripts*. Luego, dentro de la nueva carpeta de scripts, crea un nuevo archivo llamado *main.js* y guárdalo.

2.- A continuación, abre tu archivo *index.html* e introduce el siguiente código en una nueva línea, justo antes de la etiqueta de cierre < /body>:

    <script src="scripts/main.js"></script>

3.- Esto hace básicamente el mismo trabajo que el elemento *< link>* para CSS: aplica el código JavaScript a la página, para que pueda actuar sobre el HTML (y CSS, o cualquier cosa en la página).

4.- Ahora añade el siguiente código al archivo *main.js*:

    const miTitulo = document.querySelector('h1');
    miTitulo.textContent = '¡Hello word!';


5.- Finalmente, asegúrate de que has guardado los archivos HTML y JavaScript, y abre *index.html* en el navegador. Deberías ver algo así:

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/javascript/imagenes/HELLO.png" width=500>
