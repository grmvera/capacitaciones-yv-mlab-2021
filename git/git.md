# Menú
- [Que es git](#Que-es-git)
- [Comandos de git en consola](#Comandos-de-git-en-consola)
- [Clientes git](#Clientes-git)
- [Clonación de proyecto por consola y por cliente](#Clonación-de-proyecto-por-consola-y-por-cliente)
- [Commits por consola y por cliente kraken o smart](#Commits-por-consola-y-por-cliente-kraken-o-smart)
- [Ramas de kraken](#Ramas-de-kraken)
- [Merge](#Merge)

# Que es git
Git es un proyecto de código abierto maduro y con un mantenimiento activo que desarrolló originalmente Linus Torvalds, el famoso creador del kernel del sistema operativo Linux, en 2005. Un asombroso número de proyectos de software dependen de Git para el control de versiones, incluidos proyectos comerciales y de código abierto. 

# Comandos de git en consola 

- git init
    - este comando se lo usa para general una carpeta 
    <img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/init.png" width=350>

- git status
    - este comando lo usamos para ver todo lo que contenga la caprta y si los archivos estan guardado o no 
    <img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/status.png" width=350>

- git touch
    - este comando nos crea un archivo o carpeta lo que nosotros queramos agregarle 
    <img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/uch%20.png" width=350>

- git add . 
    - nos guarda todos los archivo a la nuve 
    <img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/add.png" width=350>

- git commit -m
    - este comando es para comentar todo lo que guardamos
    <img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/commit.png" width=350>

- git push
    - nos sube lo archivo que estan guardado 
    <img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/push.png" width=350>



# Clientes git
- el git client 
<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/kraken.png" width=350>

# Clonación de proyecto por consola y por cliente
- ### consola 
    llamamos a la consola y ponemos git clone y el link que queremos comentar
<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clone%201%20.png" width=350>    

- ### cliente
    en el kraken nos vamos a la parte de file y de hay a clonar ponemeos la URl del archivo que queremos clonar y donde lo queremos ubicar 
<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clone%202.png" width=350>

# Commits por consola y por cliente kraken o smart

- commit por consola 
    ponemos cun "commit -m" y le ponemos lo que queremos comentar 

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/commit%201.png" width=350>

- commit por kraken
    selecionamos el archivo que deamos comentar y ponemos lo que queremos comentar 

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/commit%202%20.png" width=350>

# Ramas de kraken
    aqui en aplastamos en Branch y creamos una nueva rama 

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/rama1.png" width=350>

# Merge
    - aqui integramos a la rama 
<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/master.png" width=350>