# Menú
- [Origen de ECMASCript](#Origen_de_ECMASCript)
- [Evolución de ECMAScript](#Evolución_de_ECMAScript)
- [ES6 vs ES5: Novedades de ECMAScript 6](#ES6_vs_ES5:_Novedades_de_ECMAScript_6)
- [Importancia de ECMAScript en la actualidad](#Importancia_de_ECMAScript_en_la_actualidad)

# Origen de ECMASCript
ECMAScript específicamente es el estándar que a partir del año 2015 a la actualidad se encarga de regir como debe ser interpretado y funcionar el lenguaje JavaScript, siendo este (JS – JavaScript) interpretado y procesado por multitud de plataformas, entre las que se encuentran los navegadores web, NodeJS u otros ambientes como el desarrollo de aplicaciones para los distintos sistemas operativos que actualmente existen en el mercado. Los responsables de dichos navegadores y JavaScript deben encargarse de interpretar el lenguaje tal como lo fija ECMAScript.

# Evolución de ECMAScript
En el mes de diciembre del año 1995 Brendan Eich uno de los empleados de Netscape diseñó y desarrolló un lenguaje llamado “Mocha” posteriormente lo renombro a LiveScript hasta que con el transcurso del tiempo debido a que la empresa Netscape fue adquirida por Sun Microsystems como estrategia de marketing se cambió su nombre a lo que hoy en día conocemos como JavaScript.

Para el mes de marzo del año 1996, Netscape Navigator 2.0 fue lanzado al mercado con soporte para JavaScript contanto con excelentes resultados en vista de la aceptación y éxito que tuvo Javascript como lenguaje de scripting del lado del cliente para sitios web. En este mismo orden, Microsoft diseñó y desarrolló un lenguaje compatible con este, al cual dieron por nombre JScript y agregaron novedosos métodos entre ellos los de las fechas y fue incluido en el navegador Internet Explorer 3.0 de este mismo año.

A raíz de la competencia que se imponía con Microsoft y las mejoras que había presentado, el equipo de Netscape hizo alianzas con la empresa Ecma International (European Computer Manufacturer Association) una organización sin fines de lucro que se encarga de regular el funcionamiento de muchos estándares de la industria mundial, no solo en Europa si no en otros continentes. Desde entonces en el año 1997 es diseñado el estándar para JavaScript DOM (Document Object Model) a fin de evitar la incompatibilidad entre navegadores, siendo la primera versión de esta ECMA-262 bajo la ISO/IEC 16262 saliendo oficialmente el 1 de junio de este año (1997), con el transcurso del tiempo las versiones han evolucionado y los estándares de JavaScript se rigen por ECMAScript a la actualidad.

# ES6 vs ES5: Novedades de ECMAScript 6
A continuación te presentamos algunas de las novedades de ECMAScript 6 (ES6) en cuanto a es5 (ECMAScript 5):

 Función Arrow: Conocidas como expresiones lambda en C# y Java, arrows o flechas son abreviaciones de funciones utilizando el operador => Por ejemplo:

    		// ES5 
    		var sum = function(y, z){ 
    		return y+z; 
    		} 
     		// ES6
    		 var sum = (y, z) => y + z;

 - El código de la sintaxis de las clases ha sido actualizado de forma que sea más sencillo y de fácil comprensión al momento de su implementación.
 
 - Para indicar que una variable solo va a estar definida en un bloque en particular se agregara let a su inicio y al terminar el bloque la variable dejará de existir, siendo de esta forma muy útil a la hora de evitar errores desde el punto de vista lógico, en el momento en que se altere una variable que no debería haberse modificado.
 
 - Las variables constantes van definidas por const en su inicio esto permitirá prevenir que una variable declarada cambie de valor, convirtiéndola efectivamente en una constante.

# Importancia de ECMAScript en la actualidad
Como hemos podido notar a lo largo de este artículo, desde que inició hasta la actualidad ECMAScript, viene siendo el estándar que abarca la web en general, todo esto gracias a las mejoras continuas y a las optimizaciones que se van desarrollando con el objetivo de tener siempre un mejor rendimiento, entre otros aspectos. Hoy en día cuenta con gran importancia en el mercado, por su versatilidad JavaScript se ha convertido en un lenguaje universal y puede ser encontrado tanto a nivel móvil como de hardware, servidor, web, entre otros. Adicionalmente, cuenta con una variedad de librerías y frameworks a fin de facilitar el desarrollo de nuevos proyectos.