# Menú
- [Información de como acceder](#Información-de-como-acceder)
- [Crear repositorios](#Crear-repositorios)
- [Crear grupos](#Crear-grupos)
- [Crear subgrupos](#Crear-subgrupos)
- [Crear issues](#Crear-issues)
- [Crear labels](#Crear-labels)
- [Roles que cumplen](#Roles-que-cumplen)
- [Agregar miembros](#Agregar-miembros)
- [Crear borads y manejo de boards](#Crear-borads-y-manejo-de-boards)

# Información de como acceder
Gitlab es un servicio web de control de versiones y desarrollo de software colaborativo basado en Git. Además de gestor de repositorios, el servicio ofrece también alojamiento de wikis y un sistema de seguimiento de errores, todo ello publicado bajo una Licencia de código abierto

los pasos para entrar son:
- buscar en google GIT-LAB
- ponemos el primero q nos sale 
- iniciamos secion 
# Crear repositorios

- al incio ponemos en crear un nuevo poryecto y selecionamos si queremos hacer un poryecto en blanco 

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/proyecto1.png" width=350>

- una ves dado en hacer un proyecto en blanco, nos sale las opciones de como se va a llamar el proyecto y si lo queremos privado o publico 

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/proyecto2.png" width=350>

# Crear grupos

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/proyecto2.png" width=350>

- nos vamos a la parte de miembros y de hay ponemos en invitar al grupo nos da las opciones de poner nombre al grupo, cuales van hacer los niveles de acceso que le daremos a cada mienbro y cuanto timepo durara el grupo.
mas abajo nos sale las opciones de todos los miembros que tenemos para poder invitarles al nuevo grupo creado

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/grupo1.png" width=350>


# Crear subgrupos

1- En el panel del grupo, haga clic en el botón Nuevo subgrupo .

2- Crea un nuevo grupo como lo harías normalmente. Observe que el espacio de nombres del grupo principal inmediato se fija en Ruta del grupo . El nivel de visibilidad puede diferir del grupo principal inmediato.

3- Haga clic en el botón Crear grupo para ser redirigido a la página del panel del nuevo grupo.

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/grupo2.png" width=550>

# Crear issues 
Los problemas son el mecanismo fundamental en GitLab para colaborar en ideas, resolver problemas y planificar el trabajo.

Mediante los problemas, puede compartir y discutir propuestas (tanto antes como durante su implementación) entre usted y su equipo, y colaboradores externos.

Puede utilizar los problemas para muchos propósitos, personalizados según sus necesidades y flujo de trabajo. Los casos de uso comunes incluyen:

Discutir la implementación de una nueva idea.
Seguimiento de tareas y estado laboral.
Aceptar propuestas de funciones, preguntas, solicitudes de soporte o informes de errores.
Elaborar nuevas implementaciones de código.

# Roles que cumplen
- para poderle dar roles a un miembro devemos agregarlo y de hay nos da la opcione de ponerle como "invitado, reportero, desarrolladro y mantenedor" 

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/rol.png" width=550>

# Agregar miembros

- nos vamos a la parte de miembro y hay nos dice que pongamos el nombre del miembro que queremos agregar y que carho le daremos y por cuanto timepo tendra acceso a nuestros archivos 

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/miembros.png" width=350>

# Crear borads y manejo de boards
nos vamos a la parte de los tableros y de hay damos en crear uno nuevo hay nos dice que nombre tendra
<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/tablero.png" width=350>