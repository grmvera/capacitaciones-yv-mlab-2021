# Esplicativo de como hacer un Login con javascript 

Para realizar este login vamos a necesitar 3 lenguajes de los que seran HTML, CSS y Javascript 


---------------

En esta imagen creamos un funcion que se llame ingresar al sistema y le ponemos como parametro *(usuario, clave, sesionIniciada = false)* dentro de la funcion le ponemos un for y dentro creamos una variable (i) y le asignamos un valor de 0 y le desimos que si i es menor que *usuariosRegistrados.length* entonces i se ira aunmentando 1. dentro del for ponemos una condicion if y le decimos que si usuario es igual a usuariosRegistrados y clave es igual a usuariosRegistrados si esto se cumple entra al if y hacemos que el usuariosRegistrados tenga la secion iniciada 
y al utlimo si no se cumple todo esto entonce saltara una alarma diciendo que el "Usuario y/o contraseña no son válidos"


Despues colocamos otra funcion con el nombre de verificarInicioSesion esta funcion lo que va hacer es verificar si el usurio ya tiene una secion iniciada y si es asi lo redirecionara

para esto colocamos dentro de la funcion una variable que se llame usuarioLogeado y que esto sea igual a la base de datos de usuario logueado.
hay creamos un if y le ponemos q si usuario logueado no es igual a nulo y si usuario logueado tiene la secion iniciada y esta es igual a true entonces saldra un mensaje diciendo "Ya tiene una sesión activa, redireccionando..."


<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/javascript/imagenes/java1.png" width=500>

creamos otra funcion con el nombre de obtenerusurio esto lo que va hacer es quemar un usuario para que este entre sin previo haber creado un usurio 

oara esto colocamos un usuariosRegistrados y que esto sea igual a usuariosRegistrados
de hay un in y si usuariosRegistrados == null o usuariosRegistrados.length == 0
entonces colocamos el usurio con el nombre que queremos y la clave

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/javascript/imagenes/java2.png" width=500>

de hay creamos una funcion cargarDatosUsuario esto nos sirve para cargar todos los datos que regitro el usurio 

hacemos otra funcion y le ponemos cerrarSesion lo que hace esto es cerrarnos la seccion cuando ya queramos salirnos sin que nos redirecione 

hacemos ora funcion con verificarInicioSesion esto sirve para que si alguien que no a iniciado secion lo redirecione al incio y para poder pasar tiene que registrarse 


<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/javascript/imagenes/java3.png" width=500>

de hay hacemos una funcion de crearUusurio esto lo que va hacer es que las personas creen usurios nuevos


<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/javascript/imagenes/java4.png" width=500>

hacemos otra funcion y a esta le ponemos actualizarUsuario lo que va hacer aqui es que si alguien que se registro escribio mal tiene la forma de corregirlos sin problema

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/javascript/imagenes/java5.png" width=500>

creamos otra funcion y a esta le ponemos de nombre eliminarUsuario esta funcion nos sirve para que si un usurio quiere eliminar su cuenta lo pueda hacer sin problema 


de hay creamos otra funcion de editarUsuario esta funcion hace que al usurio cuando aplate el boton toda su informacion se trasladara a la parte arriba para que el lo pueda ediitar sin probleble 

al ultimo llamamos a las funciones por que si no las llamamos el aplicativo no funcionaria

<img src="https://gitlab.com/grmvera/capacitaciones-yv-mlab-2021/-/raw/javascript/imagenes/java6.png" width=500>